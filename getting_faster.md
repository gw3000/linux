# Linux with Speed in the Shell

Things you need to know to get much more faster in the shell

## SUDO

```bash
find /var/lib/docker
/var/lib/docker
find: ‘/var/lib/docker’: Keine Berechtigung
```

To solve this problem you don't need to retype the line or do things like
```Cursor Up```, you just have to type:

```bash
sudo !!
```
or with the __zsh__ and __oh-my-zsh__ extension just:

```bash
_ !!
```

# Killing and yanking text

```bash
find /usr/bin -type f -mtime +1 | xargs ls -lah | awk '{print $9}'
```

To cut the command at the xargs, you don't have to got to the *xargs* and
press __delete__ until everything is deleted, just type ```CTRL+K```.

To yank the text back type ```CTRL+Y```

Cutting to the beginning of the line: ```CTRL+U```

To kill the last word from the right side (backwards) is ```CTRL+W```.

## Replace less with tail

```bash
tail -f /var/log/lastlog
```

.... is the old way. The better way is:

```bash
less +F /var(log/lastlog
```

If you wanna detach less from the end of the file you have to hit ```CTRL+C```
then you can scroll thru the file as you might know!

To reatch less to the end of the file  hit ```Shift+F```l

## Editing the current command in a text editor

If you want to edit the current command not in the shell, you wann switch over
to an editor you can do it this way:

```bash
for host in $HOSTS;do
```

... hit ```CTRL+x+e```. This will open vim to edit the current command. To
leave vim do it the common way by hitting ```:wq```.


## Paste the argument of the previous command

```bash
ping 8.8.8.8
```

in the next command you want to use the previous argument (in that case 8.8.8.8)
just hit ```ALT .```

```bash
mtr <ALT .>
```

You'll get:

```bash
mtr 8.8.8.8
```


## Unbork your terminal

```bash
reset
```
